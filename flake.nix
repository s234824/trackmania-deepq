{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/23.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {self, nixpkgs, utils, ...}:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
      in
        {
          devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [
              (pkgs.python310.withPackages (ps: with ps; [
                tensorflow
                keras
                opencv4
              ]))
            ];
          };
        });
}
