bool is_paused = false;
int checkpoint = 0;
bool finished = false;

void OnRunStep(SimulationManager@ simManager)
{
    CommandList script("script.txt");
    if (script.Content != "") {
        // Check if the speed will be 0
        int speed_Index = script.Content.FindFirst("speed");
        string speed_num = script.Content.Substr(speed_Index+6, 1);
        if (speed_Index > -1 && speed_num == "0") {
            is_paused = true;
        }
        script.Process();
        SetCurrentCommandList(script);
        script.Content = "";
        script.Save("script.txt");
    }
    Save(simManager);
}

void Render() 
{
    if (is_paused) { // If the speed is 0 only the Render() callback function will run 
        CommandList script("script.txt");
        if (script.Content != "") {
            script.Process();
            SetCurrentCommandList(script);
            script.Content = "";
            script.Save("script.txt");
            is_paused = false;
        }
    }
}

void OnCheckpointCountChanged(SimulationManager@ simManager, int count, int target)
{
    checkpoint = count;
    finished = CarFinished(simManager);
}

void Main(SimulationManager@ simManager)
{
    log("Started script");
}

bool CarFinished(SimulationManager@ simManager)
{
    TM::PlayerInfo@ playerInfo = simManager.get_PlayerInfo();
    return playerInfo.RaceFinished;
}

int CarSpeed(SimulationManager@ simManager)
{
    TM::PlayerInfo@ playerInfo = simManager.get_PlayerInfo();
    return playerInfo.DisplaySpeed;
}

void Save(SimulationManager@ simManager)
{
    CommandList input; // We write to a CommandList to send information to python 
    int speed = CarSpeed(simManager);
    input.Content = Text::FormatInt(speed) + "," +
                    Text::FormatInt(checkpoint) + "," +
                    finished;
    try
    {
        CommandList dummyFile("dummy.txt");
        dummyFile.Content;
    }
    catch
    {
        CommandList dummyFile;
        input.Save("input.txt"); 
        dummyFile.Save("dummy.txt");
    }
    pause();
}

void pause()
{
    CommandList pause_command;
    pause_command.Content = "set speed 0";
    pause_command.Process();
    SetCurrentCommandList(pause_command);
    is_paused = true;
}

PluginInfo@ GetPluginInfo()
{
    auto info = PluginInfo();
    info.Name = "AI";
    info.Author = "Marcus";
    info.Version = "v1.0.0";
    info.Description = "A AI plugin made for a project";
    return info;
}