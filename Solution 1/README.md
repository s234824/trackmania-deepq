Run order:
QueueManager.py -> async_TM_client.py -> DQN_TM_KERAS.py
train.py can also run parallel with the others

First time runing the scripts, the loadModel bool should be set to False, in hyper_params.py.

Remember to install the libraries :)

You need to make these five folders if you don't already have them:
1. models/
2. models/current model/
3. models/current model/model_primary/
4. models/current model/model_target/
5. experienceBuffer/

The rest should be fine.

test model.py is to load a model take performance data for it.

data processing.py is to process the data taken from test model.py.

neuralNetwork.py is where we have different kind of network models we have tested.

Tjek buffer.txt for a guide to download the experienceBuffer
