import tensorflow as tf
from keras_radam import RAdam
import numpy as np

from hyper_params import *
import time
from enum import Enum

import pandas
import matplotlib.pyplot as plt
import mdock

import win32gui
import win32con
import win32ui
import cv2
import os

from multiprocessing.managers import BaseManager

class QueueManager(BaseManager): pass

QueueManager.register('get_info_queue')
QueueManager.register('get_console_queue')
QueueManager.register('get_drive_queue')

hwnd = win32gui.FindWindow(None, TM_WINDOW_NAME)
win32gui.SetForegroundWindow(hwnd)

episode_reward = 0
history_dict = {"episode_reward": [], "checkpoint_ratio": [], "race_time": []}
race_count = 0

pov = 1

startAction = 11

leftBool       = False
rightBool      = False
brakeBool      = False
accelerateBool = False
currentAction  = startAction


def reset_actions():
    global leftBool, rightBool, brakeBool, accelerateBool
    leftBool       = False
    rightBool      = False
    brakeBool      = False
    accelerateBool = False


class Action(Enum):
    ACCEL               = 0
    ACCEL_LEFT          = 1
    ACCEL_RIGHT         = 2
    ACCEL_BRAKE         = 3
    LEFT                = 4
    RIGHT               = 5
    ACCEL_LEFT_BRAKE    = 6
    ACCEL_RIGHT_BRAKE   = 7
    BRAKE_LEFT          = 8
    BRAKE_RIGHT         = 9
    BRAKE               = 10
    WAIT                = 11


def action_to_command(action: Action):
    global leftBool, rightBool, brakeBool, accelerateBool

    if action == Action.ACCEL:
        leftBool        = False
        rightBool       = False
        brakeBool       = False
        accelerateBool  = True
    elif action == Action.ACCEL_LEFT:
        leftBool        = True
        rightBool       = False
        brakeBool       = False
        accelerateBool  = True
    elif action == Action.ACCEL_RIGHT:
        leftBool        = False
        rightBool       = True
        brakeBool       = False
        accelerateBool  = True
    elif action == Action.ACCEL_BRAKE:
        leftBool        = False
        rightBool       = False
        brakeBool       = True
        accelerateBool  = True
    elif action == Action.LEFT:
        leftBool        = True
        rightBool       = False
        brakeBool       = False
        accelerateBool  = False
    elif action == Action.RIGHT:
        leftBool        = False
        rightBool       = True
        brakeBool       = False
        accelerateBool  = False
    elif action == Action.ACCEL_LEFT_BRAKE:
        leftBool        = True
        rightBool       = False
        brakeBool       = True
        accelerateBool  = True
    elif action == Action.ACCEL_RIGHT_BRAKE:
        leftBool        = False
        rightBool       = True
        brakeBool       = True
        accelerateBool  = True
    elif action == Action.BRAKE_LEFT:
        leftBool        = True
        rightBool       = False
        brakeBool       = True
        accelerateBool  = False
    elif action == Action.BRAKE_RIGHT:
        leftBool        = False
        rightBool       = True
        brakeBool       = True
        accelerateBool  = False
    elif action == Action.BRAKE:
        leftBool        = False
        rightBool       = False
        brakeBool       = True
        accelerateBool  = False
    elif action == Action.WAIT:
        leftBool        = False
        rightBool       = False
        brakeBool       = False
        accelerateBool  = False

def send_action():
    global queue_drive, leftBool, rightBool, brakeBool, accelerateBool
    if queue_drive.empty():
        queue_drive.put([leftBool, rightBool, brakeBool, accelerateBool])


def rewardFunction(_speed, _distance_last_7, _cp_change, _currentAction):
    time_reward  = -1
    speed_reward = _speed/500

    distance_reward = _distance_last_7 / 100

    action_reward = 0
    if accelerateBool:
        action_reward = 0.25
    
    cp_reward = _cp_change * 50

    return time_reward + action_reward + speed_reward + cp_reward + distance_reward


def take_screenshot(_hwnd):
    left,top,right,bottom = win32gui.GetWindowRect(_hwnd)
    height = int((bottom - top) * DISPLAY_SCALING)
    width  = int((right - left) * DISPLAY_SCALING)
    tm_dc_handle = win32gui.GetWindowDC(_hwnd)
    tm_dc = win32ui.CreateDCFromHandle(tm_dc_handle)
    screenshot_dc = tm_dc.CreateCompatibleDC(tm_dc)

    bitmap = win32ui.CreateBitmap()

    bitmap.CreateCompatibleBitmap(tm_dc, width, height)

    screenshot_dc.SelectObject(bitmap)

    screenshot_dc.BitBlt((0,0), (width, height), tm_dc, (0,0), win32con.SRCCOPY)
    bitmap.Paint(screenshot_dc)

    bitmap_info  = bitmap.GetInfo()
    bitmap_str   = bitmap.GetBitmapBits(True)
    bitmap_array = np.frombuffer(bitmap_str, dtype=np.uint8)

    bitmap_array.shape = (bitmap_info['bmHeight'], bitmap_info['bmWidth'], 4)
    bitmap_array = bitmap_array[..., :3]

    downscaled = cv2.resize(bitmap_array, dsize=(IMAGE_WIDTH, IMAGE_HEIGHT), interpolation=cv2.INTER_AREA)
    downscaled = downscaled.sum(axis=2)/(255*3)

    win32gui.DeleteObject(bitmap.GetHandle())
    tm_dc.DeleteDC()
    screenshot_dc.DeleteDC()
    win32gui.ReleaseDC(hwnd, tm_dc_handle)

    return downscaled


if __name__ == '__main__':
    manager = QueueManager(address=('localhost', LOCAL_HOST_ADDRESS), authkey=KEY)
    manager.connect()
    queue_info    = manager.get_info_queue()
    queue_console = manager.get_console_queue()
    queue_drive   = manager.get_drive_queue()

    #model = tf.keras.models.load_model(NETWORK_PATH_512+"model_target", compile=True, custom_objects={'RAdam': RAdam})
    #model.summary()

    old_checkpoints_states = queue_info.get()[1]

    queue_console.put("press delete")
    reset_actions()
    send_action()

    code_speed = 50 / 1000 #ms to seconds
    code_time = time.time()
    test = time.time()
    
    time.sleep(0.9)

    start_time = time.time()
    while race_count < 100:
        image = take_screenshot(hwnd)
        image_tensor = tf.convert_to_tensor(image, dtype=tf.float32)
        image_tensor = tf.expand_dims(image_tensor,0)

        speed, checkpoint_states, distance_last_7 = queue_info.get()

        checkpoint_ratio = np.sum(checkpoint_states)/len(checkpoint_states)

        #action_probs = model([image_tensor])

        #action_index = tf.argmax(action_probs[0]).numpy()
        action_index = np.random.choice(NUM_ACTIONS)
        currentAction = action_index

        if np.sum(checkpoint_states) - np.sum(old_checkpoints_states):
            checkpoint_change = 1
        else:
            checkpoint_change = 0

        time.sleep(0.02)
        next_speed, _, next_distance_last_7 = queue_info.get()
        reward = rewardFunction(next_speed, next_distance_last_7, checkpoint_change, currentAction)
        episode_reward += reward
        old_checkpoints_states = checkpoint_states

        action_to_command(Action(currentAction))
        send_action()

        if time.time()-start_time > RESTART_TIME/GAME_SPEED:
            queue_console.put("press delete")
            reset_actions()
            send_action()
            history_dict["episode_reward"].append(episode_reward)
            history_dict["checkpoint_ratio"].append(checkpoint_ratio)
            history_dict["race_time"].append(30)
            episode_reward = 0
            plt.plot(range(len(history_dict["episode_reward"])), history_dict["episode_reward"])
            mdock.drawnow()
            time.sleep(0.7)
            start_time = time.time()
            race_count += 1

        if checkpoint_ratio == 1 and time.time()-start_time > 5/GAME_SPEED:
            queue_console.put("press delete")
            history_dict["episode_reward"].append(episode_reward)
            history_dict["checkpoint_ratio"].append(checkpoint_ratio)
            history_dict["race_time"].append(time.time()-start_time)
            episode_reward = 0
            plt.plot(range(len(history_dict["episode_reward"])), history_dict["episode_reward"])
            reset_actions()
            send_action()
            mdock.drawnow()
            time.sleep(0.7)
            start_time = time.time()
            race_count += 1

print(len(history_dict["episode_reward"]))
print(history_dict)
dataframe = pandas.DataFrame([history_dict], columns=["episode_reward", "checkpoint_ratio", "race_time"])
dataframe.to_csv(f"results/random_{pov}.csv", mode = "w", index = False, header=True)

