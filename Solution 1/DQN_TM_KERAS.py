import tensorflow as tf
from keras_radam import RAdam

from hyper_params import *

import mdock
import matplotlib.pyplot as plt

import win32gui
import win32con
import win32ui
import cv2
import os

import numpy as np
from PIL import Image
import glob
import pandas

import time
from enum import Enum

from multiprocessing.managers import BaseManager

from neuralNetwork import newNetwork_7 as network


class QueueManager(BaseManager): pass


QueueManager.register('get_info_queue')
QueueManager.register('get_console_queue')
QueueManager.register('get_train_queue')
QueueManager.register('get_drive_queue')

speedValueFactor = 400  # Skalerer speed værdien for at den ligger i samme størrelses orden som resten af input
frame_count = 0
distance_history = []
episode_reward_history = []
episode_reward = 0

hwnd = win32gui.FindWindow(None, TM_WINDOW_NAME)
win32gui.SetForegroundWindow(hwnd)

startAction = 11

leftBool       = False
rightBool      = False
brakeBool      = False
accelerateBool = False
currentAction  = startAction


def reset_actions():
    global leftBool, rightBool, brakeBool, accelerateBool
    leftBool       = False
    rightBool      = False
    brakeBool      = False
    accelerateBool = False


class Action(Enum):
    ACCEL               = 0
    ACCEL_LEFT          = 1
    ACCEL_RIGHT         = 2
    ACCEL_BRAKE         = 3
    LEFT                = 4
    RIGHT               = 5
    ACCEL_LEFT_BRAKE    = 6
    ACCEL_RIGHT_BRAKE   = 7
    BRAKE_LEFT          = 8
    BRAKE_RIGHT         = 9
    BRAKE               = 10
    WAIT                = 11


def action_to_command(action: Action):
    global leftBool, rightBool, brakeBool, accelerateBool

    if action == Action.ACCEL:
        leftBool        = False
        rightBool       = False
        brakeBool       = False
        accelerateBool  = True
    elif action == Action.ACCEL_LEFT:
        leftBool        = True
        rightBool       = False
        brakeBool       = False
        accelerateBool  = True
    elif action == Action.ACCEL_RIGHT:
        leftBool        = False
        rightBool       = True
        brakeBool       = False
        accelerateBool  = True
    elif action == Action.ACCEL_BRAKE:
        leftBool        = False
        rightBool       = False
        brakeBool       = True
        accelerateBool  = True
    elif action == Action.LEFT:
        leftBool        = True
        rightBool       = False
        brakeBool       = False
        accelerateBool  = False
    elif action == Action.RIGHT:
        leftBool        = False
        rightBool       = True
        brakeBool       = False
        accelerateBool  = False
    elif action == Action.ACCEL_LEFT_BRAKE:
        leftBool        = True
        rightBool       = False
        brakeBool       = True
        accelerateBool  = True
    elif action == Action.ACCEL_RIGHT_BRAKE:
        leftBool        = False
        rightBool       = True
        brakeBool       = True
        accelerateBool  = True
    elif action == Action.BRAKE_LEFT:
        leftBool        = True
        rightBool       = False
        brakeBool       = True
        accelerateBool  = False
    elif action == Action.BRAKE_RIGHT:
        leftBool        = False
        rightBool       = True
        brakeBool       = True
        accelerateBool  = False
    elif action == Action.BRAKE:
        leftBool        = False
        rightBool       = False
        brakeBool       = True
        accelerateBool  = False
    elif action == Action.WAIT:
        leftBool        = False
        rightBool       = False
        brakeBool       = False
        accelerateBool  = False


def send_action():
    global queue_drive, leftBool, rightBool, brakeBool, accelerateBool
    if queue_drive.empty():
        queue_drive.put([leftBool, rightBool, brakeBool, accelerateBool])


def rewardFunction(_speed, _distance_last_7, _cp_change, _currentAction):
    time_reward  = -1
    speed_reward = _speed/500

    distance_reward = _distance_last_7 / 100

    action_reward = 0
    if accelerateBool:
        action_reward = 0.25
    
    cp_reward = _cp_change * 50

    return time_reward + action_reward + speed_reward + cp_reward + distance_reward


def take_screenshot(_hwnd):
    left,top,right,bottom = win32gui.GetWindowRect(_hwnd)
    height = int((bottom - top) * DISPLAY_SCALING)
    width  = int((right - left) * DISPLAY_SCALING)
    tm_dc_handle = win32gui.GetWindowDC(_hwnd)
    tm_dc = win32ui.CreateDCFromHandle(tm_dc_handle)
    screenshot_dc = tm_dc.CreateCompatibleDC(tm_dc)

    bitmap = win32ui.CreateBitmap()

    bitmap.CreateCompatibleBitmap(tm_dc, width, height)

    screenshot_dc.SelectObject(bitmap)

    screenshot_dc.BitBlt((0,0), (width, height), tm_dc, (0,0), win32con.SRCCOPY)
    bitmap.Paint(screenshot_dc)

    bitmap_info  = bitmap.GetInfo()
    bitmap_str   = bitmap.GetBitmapBits(True)
    bitmap_array = np.frombuffer(bitmap_str, dtype=np.uint8)

    bitmap_array.shape = (bitmap_info['bmHeight'], bitmap_info['bmWidth'], 4)
    bitmap_array = bitmap_array[..., :3]

    downscaled = cv2.resize(bitmap_array, dsize=(IMAGE_WIDTH, IMAGE_HEIGHT), interpolation=cv2.INTER_AREA)
    downscaled = downscaled.sum(axis=2)/(255*3)

    win32gui.DeleteObject(bitmap.GetHandle())
    tm_dc.DeleteDC()
    screenshot_dc.DeleteDC()
    win32gui.ReleaseDC(hwnd, tm_dc_handle)

    return downscaled


if __name__ == '__main__':
    manager = QueueManager(address=('localhost', LOCAL_HOST_ADDRESS), authkey=KEY)
    manager.connect()
    queue_info    = manager.get_info_queue()
    queue_console = manager.get_console_queue()
    queue_train   = manager.get_train_queue()
    queue_drive   = manager.get_drive_queue()

    #Find counter for save files
    print("Finding new experience buffer count!")
    currentBuffer = glob.glob(EXPERIENCE_BUFFER_PATH+"*.bmp")
    if currentBuffer:
        currentExpNumber = int(currentBuffer[-1].split("\\")[-1].split(".")[0]) + 1
    else:
        currentExpNumber = 0
    print(f"New experience count found!: {currentExpNumber}")

    #load or make new model
    if LOAD_MODEL == True:
        model_target = tf.keras.models.load_model(NETWORK_PATH+"model_target", custom_objects={'RAdam': RAdam})
        model_target.compile()
    else:
        model_target = network()
        #model_target.save(NETWORK_PATH+"/model_target")

    time.sleep(0.5)
    old_checkpoints_states = queue_info.get()[1]


    queue_console.put("press delete")
    reset_actions()
    send_action()

    code_speed = 50 / 1000 #ms to seconds
    code_time = time.time()
    test = time.time()
    start_time = time.time()

    time.sleep(0.9)
    #While running!
    while True:
        #Get state
        fileName = str(currentExpNumber)
        fileName = EXPERIENCE_BUFFER_PATH + (7-len(fileName))*"0"+fileName+".bmp"

        image = take_screenshot(hwnd)
        image_tensor = tf.convert_to_tensor(image, dtype=tf.float32)
        image_tensor = tf.expand_dims(image_tensor,0)

        speed, checkpoint_states, distance_last_7 = queue_info.get()

        info_dict = {"image": fileName, "speed": speed/speedValueFactor, "checkpoint": checkpoint_states, "action": 8, "reward": -1}

        im = Image.fromarray(np.uint8(image * 255), 'L')
        im.save(fileName)
        checkpoint_ratio = np.sum(checkpoint_states)/len(checkpoint_states)
        #epsilon greedy
        if frame_count < EPSILON_RANDOM_FRAMES or (EPSILON > np.random.rand(1)[0] and frame_count < EPSILON_GREEDY_FRAMES):
            action_index = np.random.choice(NUM_ACTIONS)
        else:
            scalar_inputs = tf.convert_to_tensor(np.array(speed/speedValueFactor), dtype=tf.float32)

            scalar_inputs = tf.expand_dims(scalar_inputs, 0)
            take_time = time.time()
            action_probs = model_target([image_tensor])
            print(time.time()-take_time)
            a, b, c, d, e, f, g, h, i, j, k, l = np.round(action_probs[0], 2)
            print(["ACC:",a, "A_L",b, "A_R",c, "A_B",d, "LEF",e, "RIG",f, "A_L_B",g, "A_R_B",h, "B_L",i, "B_R",j, "BRK",k, "WAIT",l])
            print(" "*60, np.max(action_probs[0])-np.min(action_probs[0]))
            action_index = tf.argmax(action_probs[0]).numpy()
            print(" "*80, Action(action_index))
        currentAction = action_index

        EPSILON -= EPSILON_INTERVAL / EPSILON_GREEDY_FRAMES
        EPSILON = max(EPSILON, EPSILON_MIN)

        if np.sum(checkpoint_states) - np.sum(old_checkpoints_states):
            checkpoint_change = 1
        else:
            checkpoint_change = 0
        #calc reward
        time.sleep(0.02)
        next_speed, _, next_distance_last_7 = queue_info.get()
        reward = rewardFunction(next_speed, next_distance_last_7, checkpoint_change, currentAction)
        episode_reward += reward
        old_checkpoints_states = checkpoint_states

        info_dict["action"] = currentAction
        info_dict["reward"] = reward
        #save extra data to match image
        dataframe = pandas.DataFrame([info_dict], columns=["image", "speed", "checkpoint", "action", "reward"])
        if os.path.exists(EXPERIENCE_BUFFER_PATH+"metaData.csv"):
            dataframe.to_csv(EXPERIENCE_BUFFER_PATH+"metaData.csv", mode = "a", index = False, header=False)
        else:
            dataframe.to_csv(EXPERIENCE_BUFFER_PATH+"metaData.csv", mode = "a", index = False)

        action_to_command(Action(currentAction))
        send_action()

        if EPSILON < 0.1:
            EPSILON = 0.5

        if frame_count % 100 == 0:
            print(frame_count)

        if not queue_train.empty():
            model_target = tf.keras.models.load_model(NETWORK_PATH+"model_target", custom_objects={'RAdam': RAdam})
            print("should have loaded")
            queue_console.put("press delete")
            episode_reward = 0
            reset_actions()
            send_action()
            
            queue_train.get()

            time.sleep(0.7)
            start_time = time.time()

        if time.time()-start_time > RESTART_TIME/GAME_SPEED:
            queue_console.put("press delete")
            reset_actions()
            send_action()
            episode_reward_history.append(episode_reward)
            episode_reward = 0
            plt.plot(range(len(episode_reward_history)), episode_reward_history)
            mdock.drawnow()
            time.sleep(0.7)
            start_time = time.time()

        if checkpoint_ratio == 1 and time.time()-start_time > 5/GAME_SPEED:
            queue_console.put("press delete")
            episode_reward_history.append(episode_reward)
            episode_reward = 0
            plt.plot(range(len(episode_reward_history)), episode_reward_history)
            reset_actions()
            send_action()
            mdock.drawnow()
            time.sleep(0.7)
            start_time = time.time()

        while time.time()-code_time < code_speed:
            time.sleep(0)
        else:
            code_time = time.time()

        currentExpNumber += 1
        frame_count += 1
        time.sleep(0)
