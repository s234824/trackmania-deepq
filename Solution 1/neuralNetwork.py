import tensorflow as tf
from hyper_params import IMAGE_HEIGHT, IMAGE_WIDTH


def newNetwork_1():
    scalar_inputs = tf.keras.layers.Input(shape=(2,))
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH))
    
    layer1 = tf.keras.layers.Conv1D(16, kernel_size=3, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv1D(32, kernel_size=3, activation="leaky_relu")(layer1)
    layer3 = tf.keras.layers.Conv1D(64, kernel_size=3, activation="leaky_relu")(layer2)
    layer4 = tf.keras.layers.Conv1D(32, kernel_size=3, activation="leaky_relu")(layer3)
    
    layer5 = tf.keras.layers.Flatten()(layer4)
    layer6 = tf.keras.layers.Concatenate(axis=1)([layer5, scalar_inputs])
    layer7 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer6)
    layer8 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer7)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer8)
    
    return tf.keras.Model(inputs=[image_input, scalar_inputs], outputs=action_probs)


def newNetwork_2():
    scalar_inputs = tf.keras.layers.Input(shape=(1,))
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(16, kernel_size=4, strides=2, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(32, kernel_size=4, strides=2, activation="leaky_relu")(layer1)
    layer3 = tf.keras.layers.Conv2D(64, kernel_size=3, strides=2, activation="leaky_relu")(layer2)
    layer4 = tf.keras.layers.Conv2D(32, kernel_size=3, strides=1, activation="leaky_relu")(layer3)

    layer5 = tf.keras.layers.Flatten()(layer4)
    layer6 = tf.keras.layers.Concatenate(axis=1)([layer5, scalar_inputs])
    layer7 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer6)
    layer8 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer7)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer8)

    return tf.keras.Model(inputs=[image_input, scalar_inputs], outputs=action_probs)


def newNetwork_3():
    scalar_inputs = tf.keras.layers.Input(shape=(1,))
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(16, kernel_size=4, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(32, kernel_size=4, activation="leaky_relu")(layer1)
    layer3 = tf.keras.layers.MaxPool2D()(layer2)
    layer4 = tf.keras.layers.Conv2D(64, kernel_size=3, activation="leaky_relu")(layer3)
    layer5 = tf.keras.layers.Conv2D(128, kernel_size=3, activation="leaky_relu")(layer4)
    layer6 = tf.keras.layers.MaxPool2D()(layer5)

    layer7 = tf.keras.layers.Flatten()(layer6)
    layer8 = tf.keras.layers.Concatenate(axis=1)([layer7, scalar_inputs])
    layer9 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer8)
    layer10 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer9)
    layer11 = tf.keras.layers.Dense(128, activation="leaky_relu")(layer10)
    layer12 = tf.keras.layers.Dense(64, activation="leaky_relu")(layer11)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer12)

    return tf.keras.Model(inputs=[image_input, scalar_inputs], outputs=action_probs)


def newNetwork_4():
    scalar_inputs = tf.keras.layers.Input(shape=(1,))
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(32, kernel_size=(8,8), strides=(4,4), activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(64, kernel_size=(4,4), strides=(2,2), activation="leaky_relu")(layer1)
    layer3 = tf.keras.layers.Conv2D(64, kernel_size=(3,3), strides=(1,1), activation="leaky_relu")(layer2)

    layer4 = tf.keras.layers.Flatten()(layer3)
    layer5 = tf.keras.layers.Concatenate(axis=1)([layer4, scalar_inputs])
    layer6 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer5)
    layer7 = tf.keras.layers.Dense(128, activation="leaky_relu")(layer6)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer7)

    return tf.keras.Model(inputs=[image_input, scalar_inputs], outputs=action_probs)


def newNetwork_5():
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(32, kernel_size=(4,4), strides=(2,2), activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(64, kernel_size=(4,4), strides=(2,2), activation="leaky_relu")(layer1)
    sub_layer1 = tf.keras.layers.MaxPool2D()(layer2)
    layer3 = tf.keras.layers.Conv2D(64, kernel_size=(3,3), strides=(1,1), activation="leaky_relu")(sub_layer1)

    layer4 = tf.keras.layers.Flatten()(layer3)
    layer5 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer4)
    layer6 = tf.keras.layers.Dense(128, activation="leaky_relu")(layer5)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer6)

    return tf.keras.Model(inputs=[image_input], outputs=action_probs)


def newNetwork_6():
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(32, kernel_size=(8,8), strides=4, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(64, kernel_size=(8,8), strides=4, activation="leaky_relu")(layer1)
    layer3 = tf.keras.layers.Conv2D(128, kernel_size=(4,4), strides=2, activation="leaky_relu")(layer2)
    layer4 = tf.keras.layers.Conv2D(256, kernel_size=(3,3), strides=1, activation="leaky_relu")(layer3)

    layer5 = tf.keras.layers.Flatten()(layer4)
    layer6 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer5)
    layer7 = tf.keras.layers.Dense(128, activation="leaky_relu")(layer6)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer7)

    return tf.keras.Model(inputs=[image_input], outputs=action_probs)


def newNetwork_7():
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(32, kernel_size=(4,4), strides=2, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(64, kernel_size=(4,4), strides=2, activation="leaky_relu")(layer1)

    layer5 = tf.keras.layers.Flatten()(layer2)
    layer6 = tf.keras.layers.Dense(128, activation="leaky_relu")(layer5)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer6)

    return tf.keras.Model(inputs=[image_input], outputs=action_probs)


def newNetwork_8():
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(16, kernel_size=(4,4), strides=2, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(32, kernel_size=(4,4), strides=2, activation="leaky_relu")(layer1)
    layer3 = tf.keras.layers.Conv2D(64, kernel_size=(3,3), strides=1, activation="leaky_relu")(layer2)

    layer4 = tf.keras.layers.Flatten()(layer3)
    layer5 = tf.keras.layers.Dense(128, activation="leaky_relu")(layer4)
    layer6 = tf.keras.layers.Dense(128, activation="leaky_relu")(layer5)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer6)

    return tf.keras.Model(inputs=[image_input], outputs=action_probs)


def newNetwork_512():
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(32, kernel_size=(4,4), strides=2, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(64, kernel_size=(4,4), strides=2, activation="leaky_relu")(layer1)
    sub_layer1 = tf.keras.layers.MaxPool2D()(layer2)
    layer3 = tf.keras.layers.Conv2D(128, kernel_size=(3,3), strides=1, activation="leaky_relu")(sub_layer1)
    sub_layer2 = tf.keras.layers.MaxPool2D()(layer3)

    layer5 = tf.keras.layers.Flatten()(sub_layer2)
    layer6 = tf.keras.layers.Dense(512, activation="leaky_relu")(layer5)
    layer7 = tf.keras.layers.Dense(256, activation="leaky_relu")(layer6)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer7)

    return tf.keras.Model(inputs=[image_input], outputs=action_probs)


def newNetwork_1024():
    image_input = tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 1))

    layer1 = tf.keras.layers.Conv2D(32, kernel_size=(4,4), strides=2, activation="leaky_relu")(image_input)
    layer2 = tf.keras.layers.Conv2D(64, kernel_size=(4,4), strides=2, activation="leaky_relu")(layer1)
    sub_layer1 = tf.keras.layers.MaxPool2D()(layer2)
    layer3 = tf.keras.layers.Conv2D(128, kernel_size=(3,3), strides=1, activation="leaky_relu")(sub_layer1)
    sub_layer2 = tf.keras.layers.MaxPool2D()(layer3)

    layer5 = tf.keras.layers.Flatten()(sub_layer2)
    layer6 = tf.keras.layers.Dense(1024, activation="leaky_relu")(layer5)
    layer7 = tf.keras.layers.Dense(512, activation="leaky_relu")(layer6)
    action_probs = tf.keras.layers.Dense(12, activation="linear")(layer7)

    return tf.keras.Model(inputs=[image_input], outputs=action_probs)