from multiprocessing.managers import BaseManager

from tminterface.interface import TMInterface
from tminterface.client import Client
import signal
from time import sleep
import sys
import numpy as np
from collections import deque

from hyper_params import GAME_SPEED, LOCAL_HOST_ADDRESS, KEY

class QueueManager(BaseManager): pass

class MainClient(Client):
    def __init__(self) -> None:
        super(MainClient, self).__init__()
        self.speed = None
        self.cp_data = None
        self.finished = False

        self.leftBool = False
        self.rightBool = False
        self.brakeBool = False
        self.accelerateBool = False

        self.command = ""

        self.distance_queue = deque()
        self.total_distance_7_sec = 0

        self.race_distance = 0
        self.last_distance = 0
        self.time_difference = 0

    def on_registered(self, iface: TMInterface) -> None:
        print(f'Registered to {iface.server_name}')
        iface.set_speed(GAME_SPEED)
        iface.set_timeout(-1)
        sim_state = iface.get_simulation_state()
        self.cp_data = sim_state.cp_data.cp_states
        self.speed = sim_state.display_speed

    def on_deregistered(self, iface: TMInterface):
        print(f'Deregistered from {iface.server_name}')

    def on_checkpoint_count_changed(self, iface, current: int, target: int):
        if current == target:
            iface.prevent_simulation_finish()


    def on_run_step(self, iface, _time: int):
        if _time < 0:
            self.distance = 0
            self.leftBool = False
            self.rightBool = False
            self.brakeBool = False
            self.accelerateBool = False
            self.distance_queue = deque()
            self.total_distance_7_sec = 0

        if not hasattr(self, 'last_time'):
            self.last_time = _time  # Initialize last_time on the first tick
            self.total_distance = 0

        time_interval = ((_time - self.last_time) / 1000) / 3600  # Convert to hours
        self.last_time = _time

        sim_state = iface.get_simulation_state()
        
        self.cp_data = sim_state.cp_data.cp_states
        self.speed = sim_state.display_speed

        tick_distance = self.speed * time_interval * 1000

        self.distance_queue.append((_time, tick_distance))

        while self.distance_queue and _time - self.distance_queue[0][0] > 7000:
            self.distance_queue.popleft()

        self.total_distance_7_sec = sum(distance for _, distance in self.distance_queue)

        iface.set_input_state(left = self.leftBool, right = self.rightBool, brake = self.brakeBool, accelerate = self.accelerateBool)
        if self.command:    
            iface.execute_command(self.command)
            self.command = ""

    def state(self):
        return (self.speed, np.array(self.cp_data), self.total_distance_7_sec)
    
    def drive(self, commands):
        self.leftBool, self.rightBool, self.brakeBool, self.accelerateBool = commands

    def ex_command(self, command):
        self.command = command


def main():
    QueueManager.register('get_info_queue')
    QueueManager.register('get_console_queue')
    QueueManager.register('get_drive_queue')
    # Connect to the server and use the queue
    manager = QueueManager(address=('localhost', LOCAL_HOST_ADDRESS), authkey=KEY)
    manager.connect()
    queue_info = manager.get_info_queue()
    queue_console = manager.get_console_queue()
    queue_drive = manager.get_drive_queue()
    
    server_name = f'TMInterface0'
    print(f'Connecting to {server_name}...')
    
    iface = TMInterface(server_name)
    client = MainClient()
    iface = TMInterface(server_name)
    def handler(signum, frame):
        iface.close()
        sys.exit(0)
    
    signal.signal(signal.SIGBREAK, handler)
    signal.signal(signal.SIGINT, handler)
    iface.register(client)

    while not iface.registered:
        sleep(1)

    while iface.registered:
        if not queue_drive.empty():
            drive_commands = queue_drive.get()
            client.drive(drive_commands)
        
        if queue_info.empty():
            queue_info.put(client.state())
        
        if not queue_console.empty():
            client.ex_command(queue_console.get())
            
        sleep(0.01)


if __name__ == '__main__':
    main()