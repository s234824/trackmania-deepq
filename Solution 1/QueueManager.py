from multiprocessing.managers import BaseManager
from queue import Queue
from hyper_params import LOCAL_HOST_ADDRESS, KEY


class QueueManager(BaseManager): pass


# Create a simple queue and register it with the manager
queue_info = Queue()
queue_cons = Queue()
queue_train = Queue(maxsize=1)
queue_drive = Queue()

QueueManager.register('get_info_queue', callable=lambda: queue_info)
QueueManager.register('get_console_queue', callable=lambda: queue_cons)
QueueManager.register('get_train_queue', callable=lambda: queue_train)
QueueManager.register('get_drive_queue', callable=lambda: queue_drive)


def main():
    # Start a shared manager server and access the queue from it
    manager = QueueManager(address=('', LOCAL_HOST_ADDRESS), authkey=KEY)
    server = manager.get_server()
    print('Server is started')
    server.serve_forever()


if __name__ == '__main__':
    main()
