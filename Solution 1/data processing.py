import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

def load_csv():
    raw_data_512_1 = np.array(pd.read_csv("results/512_1.csv"))[0]
    raw_data_512_2 = np.array(pd.read_csv("results/512_2.csv"))[0]
    raw_data_512_3 = np.array(pd.read_csv("results/512_3.csv"))[0]
    raw_data_1024_1 = np.array(pd.read_csv("results/1024_1.csv"))[0]
    raw_data_1024_2 = np.array(pd.read_csv("results/1024_2.csv"))[0]
    raw_data_1024_3 = np.array(pd.read_csv("results/1024_3.csv"))[0]
    raw_data_random = np.array(pd.read_csv("results/random.csv"))[0]

    data_512_1, data_512_2, data_512_3 = [], [], []
    data_1024_1, data_1024_2, data_1024_3 = [], [], []
    data_random = []

    data_512_1.append(np.fromstring(raw_data_512_1[0][1:-1], sep=','))
    data_512_1.append(np.fromstring(raw_data_512_1[1][1:-1], sep=','))
    data_512_1.append(np.fromstring(raw_data_512_1[2][1:-1], sep=','))

    data_512_2.append(np.fromstring(raw_data_512_2[0][1:-1], sep=','))
    data_512_2.append(np.fromstring(raw_data_512_2[1][1:-1], sep=','))
    data_512_2.append(np.fromstring(raw_data_512_2[2][1:-1], sep=','))

    data_512_3.append(np.fromstring(raw_data_512_3[0][1:-1], sep=','))
    data_512_3.append(np.fromstring(raw_data_512_3[1][1:-1], sep=','))
    data_512_3.append(np.fromstring(raw_data_512_3[2][1:-1], sep=','))


    data_1024_1.append(np.fromstring(raw_data_1024_1[0][1:-1], sep=','))
    data_1024_1.append(np.fromstring(raw_data_1024_1[1][1:-1], sep=','))
    data_1024_1.append(np.fromstring(raw_data_1024_1[2][1:-1], sep=','))

    data_1024_2.append(np.fromstring(raw_data_1024_2[0][1:-1], sep=','))
    data_1024_2.append(np.fromstring(raw_data_1024_2[1][1:-1], sep=','))
    data_1024_2.append(np.fromstring(raw_data_1024_2[2][1:-1], sep=','))

    data_1024_3.append(np.fromstring(raw_data_1024_3[0][1:-1], sep=','))
    data_1024_3.append(np.fromstring(raw_data_1024_3[1][1:-1], sep=','))
    data_1024_3.append(np.fromstring(raw_data_1024_3[2][1:-1], sep=','))

    data_random.append(np.fromstring(raw_data_random[0][1:-1], sep=','))
    data_random.append(np.fromstring(raw_data_random[1][1:-1], sep=','))
    data_random.append(np.fromstring(raw_data_random[2][1:-1], sep=','))


    return (data_512_1, data_512_2, data_512_3), (data_1024_1, data_1024_2, data_1024_3), (data_random)

def variance(data):
    return 1/(len(data)-1) * np.sum( (data - np.mean(data))**2 )

plot = False

if __name__ == '__main__':

    (d512_1, d512_2, d512_3), (d1024_1, d1024_2, d1024_3), drandom = load_csv()

    
    plt.plot(range(len(d512_3[0])), d512_3[0])
    plt.plot(range(len(d1024_3[0])), d1024_3[0])
    plt.plot(range(len(drandom[0])), drandom[0])

    plt.title("Graf of network 512, 1028 and complete random actions over 100 races")
    plt.xlabel("Race nr.")
    plt.ylabel("Score")
    plt.legend(['net 512', 'net 1024', 'random'], loc="lower left")
    if plot:
        plt.show()

    plt.plot(range(len(d512_1[0])), d512_1[0])
    plt.plot(range(len(d512_2[0])), d512_2[0])
    plt.plot(range(len(d512_3[0])), d512_3[0])

    plt.title("graf of network 512 over 100 races")
    plt.xlabel("Race nr.")
    plt.ylabel("Score")
    plt.legend(['pov 1', 'pov 2', 'pov 3'], loc="lower left")
    if plot:
        plt.show()

    plt.plot(range(len(d1024_1[0])), d1024_1[0])
    plt.plot(range(len(d1024_2[0])), d1024_2[0])
    plt.plot(range(len(d1024_3[0])), d1024_3[0])

    plt.title("graf of network 1024 over 100 races")
    plt.xlabel("Race nr.")
    plt.ylabel("Score")
    plt.legend(['pov 1', 'pov 2', 'pov 3'], loc="lower left")
    if plot:
        plt.show()

    plt.plot(range(len(drandom[0])), drandom[0])

    plt.title("graf of random actions over 100 races")
    plt.xlabel("Race nr.")
    plt.ylabel("Score")
    if plot:
        plt.show()

    print("mean")
    print("d512_(1,2,3)", np.mean([d512_1[0], d512_2[0], d512_3[0]], axis=1))
    print("d51024_(1,2,3)", np.mean([d1024_1[0], d1024_2[0], d1024_3[0]], axis=1))
    print("drandom", np.mean(drandom[0]))
    print()
    print("median")
    print("d512_(1,2,3)", np.median([d512_1[0], d512_2[0], d512_3[0]], axis=1))
    print("d51024_(1,2,3)", np.median([d1024_1[0], d1024_2[0], d1024_3[0]], axis=1))
    print("drandom", np.median(drandom[0]))
    print()
    print("512_3")
    d512_3_var = variance(d512_3[0])
    print("variance", d512_3_var)
    print("standard deviation", np.sqrt(d512_3_var))
    print()
    print("1024")
    d1024_3_var = variance(d1024_3[0])
    print("variance", d1024_3_var)
    print("standard deviation", np.sqrt(d1024_3_var))
    print()
    print("random")
    drandom_var = variance(drandom[0])
    print("variance", drandom_var)
    print("standard deviation", np.sqrt(drandom_var))
    print()
    print("1024 pov 2")
    d1024_2_var = variance(d1024_2[0])
    print("variance", d1024_2_var)
    print("standard deviation", np.sqrt(d1024_2_var))
    print()
    print("512_3 time")
    print("min time", np.min(d512_3[2]))
    print()
    print("1024_3 time")
    print("min time", np.min(d1024_3[2]))
    print()
    print("random_time")
    print("min time", np.min(drandom[2]))
    print()
    print("1024_2 time")
    print("min time", np.min(d1024_2[2]))
"""
data:  a1024 and b512
t = 15.701, df = 173.22, p-value < 2.2e-16

95 percent confidence interval:
 425.2996 547.6055

sample estimates:
 mean of x  mean of y 
  65.34413 -421.10845 
"""

"""
data:  a1024 and crandom
t = 15.967, df = 121.32, p-value < 2.2e-16

95 percent confidence interval:
 379.7377 487.2301

sample estimates:
 mean of x  mean of y 
  65.34413 -368.13978 
"""
