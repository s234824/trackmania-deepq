import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import glob
import time
from keras_radam import RAdam
import pandas as pd

# import wandb
# from wandb.keras import WandbMetricsLogger, WandbModelCheckpoint

import cv2
from concurrent.futures import ThreadPoolExecutor

from neuralNetwork import newNetwork_10 as network
from hyper_params import *

from multiprocessing.managers import BaseManager

class QueueManager(BaseManager): pass

queue = True
try:
    QueueManager.register('get_train_queue')
    manager = QueueManager(address=('localhost', LOCAL_HOST_ADDRESS), authkey=KEY)
    manager.connect()
    queue_train = manager.get_train_queue()
except:
    print("No queue was found!")
    queue = False


optimizer = RAdam()
loss_function = tf.keras.losses.Huber()

#loss_history = []

if LOAD_MODEL:
    primaryModel = tf.keras.models.load_model(NETWORK_PATH_512+"model_primary", compile=True, custom_objects={'RAdam': RAdam})
    primaryModel.compile(optimizer=optimizer, loss=loss_function)

    targetModel = tf.keras.models.load_model(NETWORK_PATH_512+"model_target", compile=True, custom_objects={'RAdam': RAdam})
    targetModel.compile(optimizer=optimizer, loss=loss_function)
else:
    primaryModel = network()
    primaryModel.save(NETWORK_PATH_512+"model_primary")

    targetModel = network()
    targetModel.save(NETWORK_PATH_512+"model_target")


def load_image(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    return np.array(image)

def load_from_csv(_metaData):
    _speeds = metaData[:,1]
    _checkpoints = _metaData[:,2]
    _actions = metaData[:,3]
    _rewards = metaData[:,4]

    return (_checkpoints, _speeds, _rewards, _actions)

metaDataArray = np.array(pd.read_csv(EXPERIENCE_BUFFER_PATH+"metaData.csv"))
image_paths = metaDataArray[:,0] # glob.glob("experienceBuffer/*.bmp")

cur_buffer_size = len(metaDataArray)


image_array = np.empty((MAX_MEMORY_LENGTH, IMAGE_HEIGHT, IMAGE_WIDTH), dtype=np.uint8)

with ThreadPoolExecutor(max_workers=8) as executor:
    for i, image in enumerate(executor.map(load_image, image_paths)):
        image_array[i] = image

print(f'Buffer size: {cur_buffer_size}')
print(f'image_array is: {len(image_array)}')
        

start_time = time.time()

for train_count in range(1, TRAIN_AMOUNT+1):
    if len(image_paths) > BATCH_SIZE+1:
        
        indices = np.random.choice(range(cur_buffer_size-3), size = BATCH_SIZE, replace=False)

        image_sample = image_array[indices]
        image_next_sample = image_array[indices+1]
        
        metaData = np.array([metaDataArray[i] for i in indices])
        checkpoints, speeds, rewards, actions = load_from_csv(metaData)
        
        metaDataNext = np.array([metaDataArray[i+1] for i in indices])
        checkpointsNext, speedsNext, rewardsNext, actionsNext = load_from_csv(metaDataNext)

        image_sample = tf.convert_to_tensor(image_sample, dtype=tf.float32)
        image_sample = tf.expand_dims(image_sample, 3)
        normalized_image_tensor = image_sample / 255
        
        image_next_sample = tf.convert_to_tensor(image_next_sample, dtype=tf.float32)
        image_next_sample = tf.expand_dims(image_next_sample, 3)
        normalized_image_next_tensor = image_next_sample / 255
        
        future_rewards = targetModel([normalized_image_next_tensor])
        
        updated_q_values = rewards + GAMMA * tf.reduce_max(future_rewards, axis=1)
        
        masks = tf.one_hot(actions, NUM_ACTIONS)
        with tf.GradientTape() as tape:
            q_values = primaryModel([normalized_image_tensor])

            q_action = tf.reduce_sum(tf.multiply(q_values, masks), axis = 1)

            loss = loss_function(updated_q_values, q_action)
            print(loss)
            #loss_history.append(loss)
        
        grads = tape.gradient(loss, primaryModel.trainable_variables)
        optimizer.apply_gradients(zip(grads, primaryModel.trainable_variables))
        
        if train_count % UPDATE_TARGET_NETWORK == 0:
            #targetModel.set_weights(primaryModel.get_weights())
            primary_weights = primaryModel.get_weights()
            target_weights = targetModel.get_weights()

            new_target_weights = [0.2 * primary + 0.8 * target for primary, target in zip(primary_weights, target_weights)]
            targetModel.set_weights(new_target_weights)
            targetModel.save(NETWORK_PATH_512+"model_target")
            print("targetModel saved!")

            if queue:
                if not queue_train.full():
                    queue_train.put_nowait("New target model")

            primaryModel.save(NETWORK_PATH_512+"model_primary")
            print("primaryModel saved!")

        if train_count % UPDATE_DATASET == 0:
            newMetaDataArray = np.array(pd.read_csv(EXPERIENCE_BUFFER_PATH+"metaData.csv"))

            if len(newMetaDataArray) > cur_buffer_size:
                print("Updating meta data!")
                image_paths = newMetaDataArray[cur_buffer_size:,0]
                with ThreadPoolExecutor(max_workers=8) as executor:
                    for i, image in enumerate(executor.map(load_image, image_paths)):
                        image_array[cur_buffer_size + i] = image

                metaDataArray = newMetaDataArray
                print("Meta data updated!")
                cur_buffer_size = len(metaDataArray)
                print(f'Buffer size: {cur_buffer_size}')
        
        print(" "*80, round(train_count/TRAIN_AMOUNT*100, 2), "%")

        if train_count % TIME_UPDATE == 0:
            print("Minutes passed:", round((time.time() - start_time)/60, 0))

targetModel.save(NETWORK_PATH_512+"model_target")
print("targetModel saved!")

primaryModel.save(NETWORK_PATH_512+"model_primary")
print("primaryModel saved!")

print("DONE!")

print("Minutes passed:", round((time.time() - start_time)/60, 0))