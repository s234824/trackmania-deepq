#!/usr/bin/env python3

import torch
import torch.functional as F
from torch import Tensor
from torch.nn import Conv2d, Sequential, Linear

from dataclasses import dataclass
from enum import Enum

class Action(Enum):
    UP_PRESS    = 1
    UP_REL      = 2
    DOWN_PRESS  = 3
    DOWN_REL    = 4
    LEFT_PRESS  = 5
    LEFT_REL    = 6
    RIGHT_PRESS = 7
    RIGHT_REL   = 8

@dataclass
class Frame:
    speed: int
    checkpoints: int
    image: Tensor # full-resolution input image
    is_terminal: bool

def init_network():
    return Sequential([
        Conv2d(3, 16, 8, stride=4),
        # relu,
        Conv2d(16, 32, 4, stride=2),
        # relu,
        Linear(256, 256),
        Linear(256, len(Action)),
    ])

def infer(net, inputs, epsilon):
    if
    image_embedding = net.conv(inputs.image)
    embedding = Tensor.concat(image_embedding, Tensor([inputs.speed, inputs.checkpoints, action / len(Action)]))
    return (net.mlp(embedding))

def reward(frame):
    return speed + checkpoints * 1000

def learn(net, optimizer, replay, discount_rate, samples_n, epsilon):
    sample_idxs = [random.choice(replay) for _ in range(samples_n)]
    for idx in sample_idxs:
        (frame, action) = replay[idx]
        (next_frame, _) = replay[idx + 1]
        frame_reward = reward(frame)
        discounted_reward = frame_reward if frame.is_terminal else frame_reward + discount_rate * max([infer(net, next_frame, next_action, 0) for next_action in list(Action)])
        loss = (discounted_reward - infer(net, frame, action, epsilon))
        loss.backward()
