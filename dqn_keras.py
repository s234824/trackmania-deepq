#!/usr/bin/env python3
import tensorflow as tf
from tensorflow import Tensor
from tensorflow import keras
from tensorflow.keras import layers
import cv2
import numpy as np

from dataclasses import dataclass
from enum import Enum

from argparse import ArgumentParser
from sys import argv
from time import sleep
import os
import pathlib
import random

import win32gui
import win32ui
import win32con

class Action(Enum):
    UP_PRESS    = 0
    UP_REL      = 1
    DOWN_PRESS  = 2
    DOWN_REL    = 3
    LEFT_PRESS  = 4
    LEFT_REL    = 5
    RIGHT_PRESS = 6
    RIGHT_REL   = 7

def action_to_command(action: Action):
    if action == Action.UP_PRESS:
        return "press up"
    elif action == Action.UP_REL:
        return "rel up"
    elif action == Action.DOWN_PRESS:
        return "press down"
    elif action == Action.DOWN_REL:
        return "rel down"
    elif action == Action.LEFT_PRESS:
        return "press left"
    elif action == Action.LEFT_REL:
        return "rel left"
    elif action == Action.RIGHT_PRESS:
        return "press right"
    elif action == Action.RIGHT_REL:
        return "rel right"

cache_valid = False
cached_state = None

def get_trackmania_hwnd() -> int:
    return win32gui.FindWindow(None, "TrackMania Nations Forever (TMInterface 2.0.1)")

def take_screenshot() -> Tensor:
    hwnd = get_trackmania_hwnd()
    left,top,right,bottom = win32gui.GetWindowRect(hwnd)
    height = bottom - top
    width = right - left
    tm_dc_handle = win32gui.GetWindowDC(hwnd)
    tm_dc = win32ui.CreateDCFromHandle(tm_dc_handle)
    screenshot_dc = tm_dc.CreateCompatibleDC(tm_dc)

    bitmap = win32ui.CreateBitmap()
    bitmap.CreateCompatibleBitmap(tm_dc, width, height)

    screenshot_dc.SelectObject(bitmap)

    # i think these lines are unnecessary as we donẗ put the window in the background
    # win32gui.SetForegroundWindow(hwnd)
    # sleep(.2) #lame way to allow screen to draw before taking shot
    screenshot_dc.BitBlt((0,0),(width, height) , tm_dc, (0,0), win32con.SRCCOPY)
    bitmap.Paint(screenshot_dc)
    
    bitmap_info = bitmap.GetInfo()
    bitmap_str = bitmap.GetBitmapBits(True)
    bitmap_array = np.frombuffer(bitmap_str, dtype=np.uint8)

    bitmap_array.shape = (bitmap_info['bmHeight'], bitmap_info['bmWidth'], 4)
    bitmap_array = bitmap_array[..., :3]
    #bitmap_array = np.flip(bitmap_array, axis=0)
    downscaled = cv2.resize(bitmap_array, dsize=(160, 120), interpolation=cv2.INTER_CUBIC)
    return downscaled

def test_image():
    while True:
        screenshot = take_screenshot()
        cv2.imshow("asd", screenshot)
        cv2.waitKey(0)

def get_env_state(infile_name, dummy_name, skip_image=False) -> (any, int, int, bool):
    global cache_valid
    global cached_state
    if cache_valid:
        return cached_state
    while not os.path.isfile(dummy_name):
        sleep(0.1)
    with open(infile_name, "r") as f:
        in_text = f.read()
    pathlib.Path(infile_name).unlink()
    pathlib.Path(dummy_name).unlink()
    [speed_str, checkpoints_str, done_state_str] = in_text.split(",")
    speed = int(speed_str)
    checkpoints = int(checkpoints_str)
    done_state = done_state_str == "true"
    image = take_screenshot() if not skip_image else None
    cache_valid = True
    state = (image, speed, checkpoints, done_state)
    cached_state = state
    return state

def get_reward(state):
    (_, speed, checkpoints, done) = state
    return checkpoints * 1000 + speed

def take_env_action(action: Action, outfile_name: str, sim_speed):
    global cached_state
    global cache_valid
    cache_valid = False
    with open(outfile_name, "w") as f:
        f.write(f"{action_to_command(action)}\nset speed {sim_speed}\n")

def test_io(infile_name, dummy_name, outfile_name, sim_speed):
    while True:
        print("Getting state for making decision ...")
        (_, speed, checkpoints, done_state) = get_env_state(infile_name, dummy_name,  skip_image=True)
        print(f"Got state: speed: {speed}, checkpoints: {checkpoints}, done state: {done_state}")
        print("Getting state for reward")
        state = get_env_state(infile_name, dummy_name, skip_image=True)
        (_, speed, checkpoints, done_state) = state
        print(f"Got state: speed: {speed}, checkpoints: {checkpoints}, done state: {done_state}, reward: {reward(state)}")
        print("Making decision ... ")
        sleep(1)
        action = random.choice(list(Action))
        print(f"Made decision: {action_to_command(action)}")
        print("Taking action ...")
        take_env_action(action, outfile_name, sim_speed)

def create_q_model():
    # Network defined by the Deepmind paper
    scalar_inputs = layers.Input(shape=(2,))
    image_inputs = layers.Input(shape=(120, 160, 3,))

    # Convolutions on the frames on the screen
    layer1 = layers.Conv2D(32, 8, strides=4, activation="relu")(image_inputs)
    layer2 = layers.Conv2D(64, 4, strides=2, activation="relu")(layer1)
    layer3 = layers.Conv2D(64, 3, strides=1, activation="relu")(layer2)

    layer4 = layers.Flatten()(layer3)
    layer5 = layers.Concatenate(axis=1)([layer4, scalar_inputs])
    layer6 = layers.Dense(512, activation="relu")(layer5)
    action = layers.Dense(len(Action), activation="linear")(layer6)

    return keras.Model(inputs=[image_inputs, scalar_inputs], outputs=action)

def predict(state, model, training):
    (image, speed, checkpoints, done) = state
    action_probs = model([tf.reshape(image, (1, 120, 160, 3)), 
                          tf.reshape(tf.convert_to_tensor([speed, checkpoints]), (1, 2))], 
                         training=training)
    return action_probs

def make_single_batch(state):
    pass

def train(update_every, update_target_every, discount_rate, max_timestep_per_episode, random_choice_chance, sim_speed, infile_name, outfile_name, dummy_file_name):
    optim = keras.optimizers.Adam(learning_rate=0.00025, clipnorm=1.0)
    history_reward = []
    history_action = []
    history_images = []
    history_scalers = []
    step_count = 0
    model = create_q_model()
    target = create_q_model()
    while True: # run episode
        
        for timestep in range(0, max_timestep_per_episode):
            state = get_env_state(infile_name, dummy_file_name)
            (image, speed, checkpoints, done) = state 
            if np.random.randn(1)[0] < random_choice_chance:
                action = np.random.choice(list(Action))
            else:
                action_probs = predict(state, model, False)
                action = Action(tf.argmax(action_probs[0]))

            take_env_action(action, outfile_name, sim_speed)
            reward = get_reward(get_env_state(infile_name, dummy_file_name))

            history_action.append(action)
            history_images.append(state[0])
            history_scalers.append(tf.convert_to_tensor([state[1], state[2]]))
            history_reward.append(reward)
            batch_size = 8
            step_count += 1

            if step_count % update_every == 0 and len(history_images) > batch_size:
                sample_indices = np.random.choice(range(len(history_images)-1), size=batch_size)
                sample_images = tf.convert_to_tensor([history_images[i] for i in sample_indices])
                sample_scalers = tf.convert_to_tensor([history_scalers[i] for i in sample_indices])
                sample_next_images = tf.convert_to_tensor([history_images[i+1] for i in sample_indices])
                sample_next_scalers = tf.convert_to_tensor([history_scalers[i+1] for i in sample_indices])
                sample_actions = [history_action[i].value for i in sample_indices]
                sample_rewards = [history_reward[i] for i in sample_indices]
                future_rewards = target([sample_next_images, sample_next_scalers])
                updated_q_values = sample_rewards + discount_rate * tf.reduce_max(future_rewards, axis=1)
                masks = tf.one_hot(sample_actions, len(Action))
                with tf.GradientTape() as tape:
                    q_values = model([sample_images, sample_scalers])
                    q_action = tf.reduce_sum(tf.multiply(q_values, masks))
                    loss = (updated_q_values - q_action)**2
                    print(loss, "loss")
                grads = tape.gradient(loss, model.trainable_variables)
                optim.apply_gradients(zip(grads, model.trainable_variables))
                print("Gradiant applyed")

            if step_count % update_target_every == 0:
                print("Updating weights on target")
                target.set_weights(model.get_weights())
                print("Done")

class Cmd(Enum):
    train = 1
    test_io = 2
    test_image = 3

def main():
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(required=True)

    io_parser = ArgumentParser(add_help=False)
    io_parser.add_argument("outfile")
    io_parser.add_argument("infile")
    io_parser.add_argument("dummy")

    io_test_parser = subparsers.add_parser("test-io", parents=[io_parser])
    io_test_parser.set_defaults(cmd=Cmd.test_io)

    train_args_parse = subparsers.add_parser("train", parents=[io_parser])
    train_args_parse.add_argument("--discount-rate", default=0.01, type=float)
    train_args_parse.add_argument("--sim-speed", default=1, type=float)
    train_args_parse.add_argument("--max-episode-length", default=10_000, type=int)
    train_args_parse.add_argument("--epsilon", default=0.01, type=float)
    train_args_parse.add_argument("--update-every", default=4, type=int)
    train_args_parse.add_argument("--update-target-every", default=128, type=int)
    train_args_parse.set_defaults(cmd=Cmd.train)

    test_image_arg_parse = subparsers.add_parser("test-image")
    test_image_arg_parse.set_defaults(cmd=Cmd.test_image)

    args = parser.parse_args()

    if args.cmd == Cmd.test_io:
        test_io(args.infile_name, args.dummy_name, args.outfile_name, args.sim_speed)
    elif args.cmd == Cmd.test_image:
        test_image()
    elif args.cmd == Cmd.train:
        train(args.update_every,
              args.update_target_every,
              args.discount_rate,
              args.max_episode_length,
              args.epsilon,
              args.sim_speed,
              args.infile,
              args.outfile,
              args.dummy)
    else:
        raise Exception("invalid command")

if __name__ == "__main__":
    main()